package domain;

import java.util.Scanner;

public class Arvore {

	private static Scanner scan = new Scanner(System.in);

	private static class ARVORE {
		public int numero;
		public ARVORE direita, esquerda;
	}

	public static void main(String[] args) {
		ARVORE raiz = null;
		Integer esc, numero, achou;

		do {
			System.out.println("(1) Preenche a árvore");
			System.out.println("(2) Imprimir os pares");
			System.out.println("(3) Imprimir os ímpares");
			System.out.println("(4) Consultar a árvore em ordem");
			System.out.println("(5) Imprimir os números primos");
			System.out.println("(6) Imprimir pré-ordem e pós-ordem");
			System.out.println("(7) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch(esc) {
			case 1: {
				raiz = null;
				for(int i = 1; i <= 10; i++) {
					System.out.println("Informe o número que deseja inserir: ");
					numero = scan.nextInt();
					scan.nextLine();

					raiz = inserir(raiz, numero);
				}
				break;
			}
			case 2: {
				if(raiz == null) {
					System.out.println("Sua árvore está vazia, utilize a opção (1) para preencher...");
				} else {
					achou = 0;
					achou = consultarPares(raiz, achou);
					if(achou == 0) {
						System.out.println("Não encontrei pares!");
					}
				}
				break;
			}
			case 3: {
				if(raiz == null) {
					System.out.println("Sua árvore está vazia, utilize a opção (1) para preencher...");
				} else {
					achou = 1;
					achou = consultarImpares(raiz, achou);
					if(achou == 1) {
						System.out.println("Não encontrei ímpares!");
					}
				}
				break;
			} 
			case 4:{
				if(raiz == null) {
					System.out.println("Sua árvore está vazia!");
				} else {
					System.out.println("Os números da árvore são: ");
					consultarEmOrdem(raiz);
				}
				break;
			}
			case 5: {
				if (raiz == null) {
					System.out.println("Sua árvore está vazia, utilize a opção (1) para preencher...");
				} else {
					achou = 0;
					achou = consultarPrimos(raiz, achou);
					if (achou == 0) {
						System.out.println("Não há números primos!");
					}
				}
				break;
			}
			case 6: {
				
				break;
			}
			}
		} while(esc != 7);
	}

	public static ARVORE inserir(ARVORE aux, Integer num) {
		if(aux == null) {
			aux = new ARVORE();
			aux.numero = num;
			aux.direita = null;
			aux.esquerda = null;
		} else if(num < aux.numero) {
			aux.esquerda = inserir(aux.esquerda, num);
		} else {
			aux.direita = inserir(aux.direita, num);
		}
		return aux;
	}

	public static Integer consultarPares(ARVORE aux, Integer achou) {
		if(aux != null) {
			if(aux.numero %2 == 0) {
				System.out.println("Os números pares são: " + aux.numero + " ");
				achou = 1;
			}
			achou = consultarPares(aux.esquerda, achou);
			achou = consultarPares(aux.direita, achou);
		}
		return achou;
	}

	public static Integer consultarImpares(ARVORE aux, Integer achou) {
		if (aux != null) {
			if (aux.numero % 2 != 0) {
				System.out.println("Os números ímpares são: " + aux.numero + " ");
				achou = 1;
			}
			achou = consultarImpares(aux.esquerda, achou);
			achou = consultarImpares(aux.direita, achou);
		}
		return achou;
	}

	public static int consultarPrimos(ARVORE aux, int achou) {
		if (aux != null) {
			int cont = 0;
			for (int i = 2; i <= aux.numero; i++) {
				if (aux.numero % i == 0) {
					cont++;
				}
			}
			if (cont == 1) {
				System.out.println("Número primo: " + aux.numero);
				achou = 1;
			}
			achou = consultarPrimos(aux.esquerda, achou);
			achou = consultarPrimos(aux.direita, achou);
		}
		return achou;
	}

	public static void consultarEmOrdem(ARVORE aux) {
		if(aux != null) {
			consultarEmOrdem(aux.esquerda);
			System.out.println(aux.numero);
			consultarEmOrdem(aux.direita);
		}
	}

}
